package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/Nachterretten/json-rpc/config"
	"gitlab.com/Nachterretten/json-rpc/internal/db"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/cache"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/component"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/db/migrate"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/db/scanner"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/errors"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/responder"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/router"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/server"
	internal "gitlab.com/Nachterretten/json-rpc/internal/infrastructure/service"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/tools/cryptography"
	"gitlab.com/Nachterretten/json-rpc/internal/modules"
	"gitlab.com/Nachterretten/json-rpc/internal/provider"
	"gitlab.com/Nachterretten/json-rpc/internal/storages"
	models2 "gitlab.com/Nachterretten/json-rpc/pkg/models"
	"gitlab.com/Nachterretten/json-rpc/rpc/grpc"
	"gitlab.com/Nachterretten/json-rpc/rpc/grpc/proto_user"
	"gitlab.com/Nachterretten/json-rpc/rpc/jrpc/user"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	grpc2 "google.golang.org/grpc"
	"net/http"
	"net/rpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server
	gRPC     server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Services *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	// запускаем сервер
	switch a.conf.RPCServer.Type {
	case "jrpc":
		errGroup.Go(func() error {
			err := a.jsonRPC.Serve(ctx)
			if err != nil && err != http.ErrServerClosed {
				a.logger.Error("app: server error", zap.Error(err))
				return err
			}
			return nil
		})
	case "grpc":
		errGroup.Go(func() error {
			err := a.gRPC.Serve(ctx)
			if err != nil && err != http.ErrServerClosed {
				a.logger.Error("app: server error", zap.Error(err))
				return err
			}
			return nil
		})
	}

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models2.UserDTO{},
		&models2.EmailVerifyDTO{},
		&models2.TestDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages
	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	a.Services = services

	switch a.conf.RPCServer.Type {
	case "jrpc":
		// инициализация сервера json RPC
		userRPC := user.NewUserServiceJSONRPC(services.User)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(userRPC)
		if err != nil {
			a.logger.Fatal("error init user json RPC", zap.Error(err))
		}
		a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)
	case "grpc":
		// инициализация сервера GRPC
		userGRPC := grpc.NewUserServiceGRPC(a.Services.User)
		GRPCServer := grpc2.NewServer()
		proto_user.RegisterUserServiceRPCServer(GRPCServer, userGRPC)
		a.gRPC = server.NewGRPCServer(a.conf.RPCServer, GRPCServer, a.logger)
	}

	controllers := modules.NewControllers(a.Services, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
