package storages

import (
	"gitlab.com/Nachterretten/json-rpc/internal/db/adapter"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/cache"
	ustorage "gitlab.com/Nachterretten/json-rpc/internal/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
	}
}
