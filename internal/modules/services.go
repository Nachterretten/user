package modules

import (
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/component"
	uservice "gitlab.com/Nachterretten/json-rpc/internal/modules/user/service"
	"gitlab.com/Nachterretten/json-rpc/internal/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}
